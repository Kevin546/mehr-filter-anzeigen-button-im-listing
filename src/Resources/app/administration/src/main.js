import template from './extension/sw-property-detail-base/sw-property-detail-base.html.twig';
const { Criteria } = Shopware.Data;

Shopware.Component.override('sw-property-detail-base', {
    template,

    inject: ['repositoryFactory', 'acl'],

    data() {
        return {
            customFieldSets: [],
            isLoading: false
        };
    },

    computed: {
        
        customFieldSetRepository() {
            return this.repositoryFactory.create('custom_field_set');
        },

        customFieldSetCriteria() {
            const criteria = new Criteria(1, 100);

            criteria.addFilter(Criteria.equals('relations.entityName', 'property_group'));
            criteria
                .getAssociation('customFields')
                .addSorting(Criteria.sort('config.customFieldPosition', 'ASC', true));

            return criteria;
        },
    },

    created() {
        this.loadEntityData();
    },

    methods: {
        loadEntityData() {
            this.isLoading = true;

            this.customFieldSetRepository
                .search(this.customFieldSetCriteria, Shopware.Context.api)
                .then((result) => {
                	this.isLoading = false;
                    this.customFieldSets = result.filter((set) => set.customFields.length > 0);
                });
        }
    }
});