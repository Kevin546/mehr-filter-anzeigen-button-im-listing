import Plugin from 'src/plugin-system/plugin.class';

export default class ExtendFilterButton extends Plugin {
    init() {
        this.activeFilter = document.querySelectorAll('.filter-active');
        this.importantPropertyFilter = document.querySelectorAll('.property-filter1');
        this.unimportantPropertyFilter = document.querySelectorAll('.property-filter');
        this.extendFilterButton = document.querySelector('.extend-filter-button');

        if(this.importantPropertyFilter.length == 0 || this.activeFilter.length > 0){
            {
                for(let i = 0; i < this.unimportantPropertyFilter.length; i++)
                {
                    this.unimportantPropertyFilter[i].style.display = "block";
                }
                this.extendFilterButton.style.display = "none";
            }
        }
        this.extendFilterButton.addEventListener('click', (e) => {
            this.showAllFilter();
         });
    }

    showAllFilter(){
        for(let i = 0; i < this.unimportantPropertyFilter.length; i++)
        {
            this.unimportantPropertyFilter[i].style.display = "block";
        }
        this.extendFilterButton.style.display = "none";
    }

}