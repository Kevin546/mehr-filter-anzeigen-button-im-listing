import ExtendFilterButton from './ExtendFilterButton/ExtendFilterButton.plugin';

const PluginManager = window.PluginManager;
PluginManager.register('ExtendFilterButton', ExtendFilterButton, '[data-filter-button-plugin]');
